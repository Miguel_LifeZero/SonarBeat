﻿using core;
using core.utilities;
using UnityEngine;
using UnityEngine.UI;

namespace actions {
    public abstract class ActionBase : MonoBehaviourBase {

        internal Button ButtonPressed;
        internal string HighlightedButton;

        private void Awake() {
            //Assign button
            ButtonPressed = AddComponent<Button>();
            ButtonPressed.onClick.AddListener(OnClickAction);
        }

        public virtual void OnClickAction() {
            ////Debug.Log("----------------click Action done--------------------");
        }

        public virtual void ChangeTabAction(string highlightedButton) {
            ////Debug.Log("----------------click Tab Action done--------------------");
        }
    }
}