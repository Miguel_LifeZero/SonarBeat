﻿using UnityEngine;
using EasySteamLeaderboard;

namespace Steamworks
{
    public class FetchLeaderboardEntries : MonoBehaviour {
        #region Variables
        public string AppId;
        public GameObject EntryPrefab;
        public GameObject Grid;
        #endregion
        
        #region GetEntryData
        public void FetchEntries() {
            EasySteamLeaderboards.Instance.GetLeaderboard("Feet Traveled", (result) => {
                if (result.resultCode == ESL_ResultCode.Success) {
                    for (var i = 0; i < result.GlobalEntries.Count; ++i) {
                        Debug.Log("Entry " + ( i + 1) + " : " + result.GlobalEntries[i].PlayerName + "-" + result.GlobalEntries[i].Score);
                        CreateEntries(i, result.GlobalEntries[i]);
                    }   
                }
                else 
                    Debug.Log("Failed fecthing data: " + result.resultCode);
            });
        }
        #endregion
        
        #region CreateEntries
        private void CreateEntries(int id, ESL_LeaderboardEntry entry) {
            var entryPosition = new Vector3(Grid.transform.position.x, -0.125f * id);
            var entryGo = Instantiate(EntryPrefab, entryPosition, Quaternion.identity, Grid.transform);
            var entryComponent = entryGo.GetComponent<Entry>();
            entryComponent.SetRank(id);
            entryComponent.SetName(entry.PlayerName);
            entryComponent.SetScore(entry.Score);
        }
        #endregion
    }
}

