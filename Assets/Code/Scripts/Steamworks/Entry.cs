﻿using UnityEngine;

namespace Steamworks
{
    public class Entry : MonoBehaviour {
        #region Variables
        public int Rank;
        public string Name;
        public string Score;

        public GameObject LabelRank, LabelName, LabelScore;
        #endregion
        
        #region Setters
        public void SetRank(int rank) {
            Rank = rank;
            Debug.Log("what is " + LabelRank);
            LabelRank.GetComponent<UILabel>().text = Rank.ToString();
        }
        
        public void SetName(string n) {
            Name = n;
            LabelName.GetComponent<UILabel>().text = Name;
        }
        
        public void SetScore(string score){
            Score = score;
            LabelScore.GetComponent<UILabel>().text = Score;
        }
        #endregion
    }
}

