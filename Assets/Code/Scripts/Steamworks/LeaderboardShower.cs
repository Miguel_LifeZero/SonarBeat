﻿using UnityEngine;
using UnityEngine.UI;

namespace Steamworks
{
    public class LeaderboardShower : MonoBehaviour {
        #region Variables
        public GameObject LeaderboardHud;
        public GameObject LeaderboardFetcher;

        private bool _windowsOn;
        #endregion
        
        #region Initializer
        private void Awake() {
            _windowsOn = false;
            var button = transform.GetComponent<Button>();
            button.onClick.AddListener(ShowOrHideLeaderboard);
        }
        #endregion

        #region ManageLeaderboard
        private void ShowOrHideLeaderboard() {
            _windowsOn = !_windowsOn;
            LeaderboardHud.SetActive(_windowsOn);
            FecthEntries();
        }

        private void FecthEntries() {
            if(_windowsOn)
                LeaderboardFetcher.GetComponent<FetchLeaderboardEntries>().FetchEntries();
        }
        #endregion
    }
}

