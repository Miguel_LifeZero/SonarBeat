﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class LoadNextScene : MonoBehaviour {
        public string Scene;
        private void Awake() {
            SceneManager.LoadScene(Scene);
        }
    }
}

