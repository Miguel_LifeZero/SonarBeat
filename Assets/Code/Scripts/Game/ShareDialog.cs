﻿using core.utilities;
using UnityEngine;
using UnityEngine.UI;
using VoxelBusters.NativePlugins;

namespace Game
{
    public class ShareDialog : MonoBehaviour 
    {
        #region Variables
        public string ShareMessage, ShareURL;
        private readonly SocialShareSheet _shareSheet = new SocialShareSheet();
        #endregion

        private void Awake()
        {
            var button = transform.GetComponent<Button>();
            button.onClick.AddListener(SetPopOver);
        }

        private void SetPopOver()
        {
            var score = Finder.FindScoreText();
            _shareSheet.Text = ShareMessage + score.text;
           // _shareSheet.URL = ShareURL;
            _shareSheet.AttachScreenShot();
            
            NPBinding.UI.SetPopoverPointAtLastTouchPosition();
            NPBinding.Sharing.ShowView(_shareSheet, FinishedSharing);
        }

        private void FinishedSharing(eShareResult result)
        {
            Debug.Log("Finished sharing - " + result );
        }
    }
}

