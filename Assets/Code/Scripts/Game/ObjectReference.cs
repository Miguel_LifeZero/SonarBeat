﻿using UnityEngine;

namespace Game
{
    public class ObjectReference : MonoBehaviour 
    {
        #region
        public GameObject RadarBase;
        public GameObject Compass;
        public GameObject EnemiesHolder;
        public GameObject ObjectType1, ObjectType2, ObjectType3;
        #endregion
    }
}

