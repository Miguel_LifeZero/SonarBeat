﻿using UnityEngine;

public class DevelopmentTest : MonoBehaviour {

	// Just for development testing. After I will have to remodify the script execution order preferences
	void Awake()
	{
		GameObject check = GameObject.Find("_gameManager");
		if (check == null)
		{ UnityEngine.SceneManagement.SceneManager.LoadScene("_preload"); }
	}
}
