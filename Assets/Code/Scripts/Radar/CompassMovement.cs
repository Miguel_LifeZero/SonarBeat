﻿using System.Collections.Generic;
using Enemies;
using UnityEngine;

namespace Radar
{
    public class CompassMovement : MonoBehaviour 
    {
        #region Variables
        public float Degrees;
        public float RotationSpeed;
        public List<GameObject> VisibleEnemies = new List<GameObject>();
        #endregion

        #region UpdateMethod
        private void Update() {
            transform.Rotate(0, 0, Degrees * Time.deltaTime * RotationSpeed);
        }
        #endregion
        
        #region OnTrigger
        private void OnTriggerEnter2D(Collider2D other) {
            if (other.GetComponent<EnemyBehavior>() == null)
                return;
            VisibleEnemies.Add(other.gameObject);
            other.GetComponent<EnemyBehavior>().ShowItself();
        }

        private void OnTriggerExit2D(Collider2D other) {
            if (other.GetComponent<EnemyBehavior>() == null)
                return;
            VisibleEnemies.Remove(other.gameObject);
            other.GetComponent<EnemyBehavior>().HideItself();
        }
        #endregion
        
        public Vector3 GetAimPoint() {
            return transform.GetChild(0).position;
        }
    }
}

