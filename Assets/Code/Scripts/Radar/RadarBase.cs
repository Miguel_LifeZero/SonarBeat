﻿using UnityEngine;

namespace Radar
{
    public class RadarBase : MonoBehaviour 
    {
        #region Variables
        private float _radius;
        private Vector3 _center;
        #endregion
        
        #region Initialize
        private void Awake() {
            CalculateRadius();
            CalculateCenter();
        }

        private void CalculateRadius() {
            _radius = GetComponent<Renderer>().bounds.size.x / 2;
        }
        
        private void CalculateCenter() {
            _center = gameObject.transform.position;
        }
        #endregion
        
        #region Getters
        public float GetRadius() {
            return _radius;
        }
        
        public Vector3 GetCenter() {
            return _center;
        }

        
        #endregion
    }
}
