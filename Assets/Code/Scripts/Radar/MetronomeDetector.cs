﻿using System.Collections.Generic;
using core;
using core.configs;
using UnityEngine;

namespace Radar {
	public class MetronomeDetector : MonoBehaviourBase {
		
		#region Variables
		public List<GameObject> _collidingBeats = new List<GameObject>();
		#endregion
		
		#region Init
		private void Awake() {
			EventDispatcher.AttackEvent += AttackLaunched;
		}

		private void OnDestroy() {
			EventDispatcher.AttackEvent -= AttackLaunched;
		}
		#endregion
		
		#region AttackEvent
		private void AttackLaunched() {
			for (var i = 0; i < _collidingBeats.Count; ++i) {
				Destroy(_collidingBeats[i]);
			}
		}
		#endregion

		#region Trigger
		private void OnTriggerEnter2D(Collider2D other) {
			if (other.gameObject.layer != TagsAndLayers.PhysicsLayers.Beats)
				return;
			_collidingBeats.Add(other.gameObject);
			EventDispatcher.OnBeatTouched();
		}

		private void OnTriggerExit2D(Collider2D other) {
			if (other.gameObject.layer != TagsAndLayers.PhysicsLayers.Beats)
				return;
			_collidingBeats.RemoveAll(x => other.gameObject);
			_collidingBeats.TrimExcess();
			EventDispatcher.OnBeatReleased();
		}
		#endregion
	}
}