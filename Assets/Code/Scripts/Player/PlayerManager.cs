﻿using core;
using core.utilities;
using Enemies;
using Game;
using Radar;
using UnityEngine;

namespace Player {
    public class PlayerManager : MonoBehaviour {
        
        #region Variables
        public int Score;

        private float _minDistance;
        private int _enemyIndex;
        private ObjectReference _objectReference;
        private InputManager _inputManager;
        private CompassMovement _compassMovement;
        #endregion

        #region Initialization
        private void Awake() {
            EventDispatcher.AttackEvent += KillScore;
        }

        private void OnDestroy() {
            EventDispatcher.AttackEvent -= KillScore;
        }
        
        private void Start() {
            Score = 0;
            _objectReference = Finder.FindObjectReference();
            _inputManager = Finder.FindInputManager();
            _compassMovement = Finder.FindCompassMovement();
        }
        #endregion

        #region ManageInputOfPlayer
        private void KillScore() {
            var listEnemies = _compassMovement.VisibleEnemies;
            if (listEnemies.Count == 0) return;
            _minDistance = 3.0f;
            _enemyIndex = 0;
            for (var i = 0; i < listEnemies.Count; ++i) {
                var enemyBehavior = listEnemies[i].GetComponent<EnemyBehavior>();
                if (!enemyBehavior.InMetronome) return;
                CalculateNearestEnemy(enemyBehavior, i);
                Debug.Log("DIST " + _minDistance);
                CalculateScore();
            }
        }

        #endregion
        
        #region ManageNearestEnemy
        private void CalculateNearestEnemy(EnemyBehavior enemyBehavior, int index) {
            var distance = enemyBehavior.CalculateDistanceToAim();
            if (distance >= _minDistance) return;
            _minDistance = distance;
            _enemyIndex = index;
        }
        #endregion
        
        #region SetScore
        private void CalculateScore() {
            if (_minDistance <= 0.56f) Score += 5;
            else Score += 1;
            UpdateScore();
        }

        private void UpdateScore(){
            Finder.FindScoreText().text = Score.ToString();
        }
        #endregion
    }
}