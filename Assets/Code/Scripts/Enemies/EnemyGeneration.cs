﻿using System.Collections.Generic;
using core.configs;
using core.utilities;
using Game;
using Radar;
using UnityEngine;

namespace Enemies {
    public class EnemyGeneration : MonoBehaviour {
        
        #region Variables
        public int NumObjects = 10;
        public int SectionsPerCycle = 4;

        private RadarBase _radarBase;
        private ObjectReference _objectReference;
        private List<int> _filledSections = new List<int>();
        #endregion

        #region SetPositionsOfObjects
        private void Start() {
            _radarBase = Finder.FindRadarBase();
            _objectReference = Finder.FindObjectReference();
            GenerateBeats();
        }

        private void GenerateBeats() {
            _filledSections.Clear();
            for (var i = 0; i < NumObjects; ++i) {
                var randomPosition = Random.Range(0, SectionsPerCycle);
                if (_filledSections.Contains(randomPosition))
                    return;
                _filledSections.Add(randomPosition);
                var angle = RadarUtils.RandomAngleInRadar(SectionsPerCycle, randomPosition);
                var enemyPosInRadar = RadarUtils.PlaceOnRadar(_radarBase.GetCenter(), angle, _radarBase.GetRadius());
                var objectPrefab = Instantiate(_objectReference.ObjectType1, enemyPosInRadar, Quaternion.identity, transform);
                SendInfoToEnemy(objectPrefab, angle, enemyPosInRadar);
                TextUtils.CleanGameobjectName(objectPrefab, Names.GameObjects.InstantiatedClone);
            }
        }
        #endregion
        
        #region EnemyInfo
        private void SendInfoToEnemy(GameObject enemy, float angle, Vector3 enemyPosInRadar) {
            var enemyBehaviour = enemy.GetComponent<EnemyBehavior>();
            enemyBehaviour.AngleInRadar = angle;
            enemyBehaviour.PositionInRadar = enemyPosInRadar;
        }
        #endregion
    }
}