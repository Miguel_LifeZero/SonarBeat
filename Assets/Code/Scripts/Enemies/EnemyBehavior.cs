﻿using System.Collections;
using core;
using core.configs;
using core.utilities;
using UnityEngine;

namespace Enemies {
    public class EnemyBehavior : MonoBehaviour {
        
        #region Variables
        public float AngleInRadar;
        public Vector3 PositionInRadar;
        public bool InMetronome;

        private float _initialDistanceFromCenter;
        private MeshRenderer _mesh;
        #endregion
        
        #region Initialize
        private void Awake() {
            EventDispatcher.BeatTouchEvent += MetronomeDetected;
            EventDispatcher.BeatReleaseEvent += MetronomeReleased;
            CacheMeshRenderer();
            CalculateInitialDistance();
        }

        private void OnDestroy() {
            EventDispatcher.BeatTouchEvent -= MetronomeDetected;
            EventDispatcher.BeatReleaseEvent -= MetronomeReleased;
        }

        private void CacheMeshRenderer() {
            _mesh = GetComponent<MeshRenderer>();
            _mesh.enabled = false;
        }

        private void CalculateInitialDistance() {
            _initialDistanceFromCenter = Vector3.Distance(Finder.FindRadarBase().GetCenter(), transform.position);
        }
        #endregion
        
        #region MetronomeDetection
        private void MetronomeDetected() {
            if(_mesh.enabled)
                InMetronome = true;
        }
        private void MetronomeReleased() {
            InMetronome = false;
        }
        #endregion
        
        #region Renderer
        public void ShowItself() {
            _mesh.enabled = true;
        }

        public void HideItself() {
            _mesh.enabled = false;
            StartCoroutine(MoveForward());
        }
        #endregion
        
        #region Trigger
        private void OnTriggerEnter2D(Collider2D other) {
            if(other.gameObject.layer == TagsAndLayers.PhysicsLayers.Metronome)
                InMetronome = true;
        }

        private void OnTriggerExit2D(Collider2D other) {
            if(other.gameObject.layer == TagsAndLayers.PhysicsLayers.Metronome)
                InMetronome = false;
        }
        #endregion
        
        #region Movement
        private IEnumerator MoveForward() {
            var startTime = Time.time;
            while (Time.time < startTime + .3f) {
                yield return null;
            }
            var radarBase = Finder.FindRadarBase();
            var currentRadius = Vector3.Distance(radarBase.GetCenter(), PositionInRadar) - _initialDistanceFromCenter * 0.2f;
            PositionInRadar = RadarUtils.PlaceOnRadar(radarBase.GetCenter(), AngleInRadar, currentRadius);
            transform.position = PositionInRadar;
        }
        #endregion
        
        #region Calculations
        public float CalculateDistanceToAim() {
            var compassMovement = Finder.FindCompassMovement();
            var aimPosition = compassMovement.GetAimPoint();
            var distance = Vector3.Distance(aimPosition, transform.position);
            return distance;
        }
        #endregion
    }
}