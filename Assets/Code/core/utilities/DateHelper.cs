﻿using System;
using System.Globalization;

namespace core.utilities {
	public class DateHelper {

		public static DateTime Now() {
			return DateTime.Now;
		}

		public static string NowDate() {
			return DateTime.Now.ToString("yyyy-MM-dd");
		}

		public static string NowDateTime() {
			return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
		}

		public static string ParseDateToMonthFirst(DateTime date) {
			var capitalizedMonth = VarHelper.FirstCharToUpper(date.ToString("MMMM"));
			var dateMonthFirst = capitalizedMonth + " " + date.ToString("dd") + " " + date.ToString("yyyy");
			return dateMonthFirst;
		}

		public static DateTime ParseDateTime(string date) {
			const string formatString = "yyyy-MM-dd HH:mm:ss";
			date = date == "0000-00-00 00:00:00" ? "1971-01-01 01:01:01" : date;
			return DateTime.ParseExact(date,
				formatString,
				CultureInfo.InvariantCulture);
		}

		public static DateTime ParseDate(string date) {
			const string formatString = "yyyy-MM-dd";
			return DateTime.ParseExact(date,
				formatString,
				CultureInfo.InvariantCulture);
		}

		public static DateTime ParseServerDateTime(string dateTime) {
			var iKnowThisIsUtc = ParseDateTime(dateTime);
			var runtimeKnowsThisIsUtc = DateTime.SpecifyKind(
				iKnowThisIsUtc,
				DateTimeKind.Utc);
			return runtimeKnowsThisIsUtc.ToLocalTime();
		}

		public static string MonthDayHourFormat(DateTime date) {
			return date.ToString("MMM d yyyy, HH:mm");
		}

		private static string NoZero(int number, string text) {
			if (number > 0) {
				return ", " + number + text;
			}
			return "";
		}

		public static int DiffInSeconds(DateTime dateTime1, DateTime dateTime2) {
			return (int) (dateTime1 - dateTime2).TotalSeconds;
		}

		public static long ToUnixTime(DateTime datetime) {
			var sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			return (long) (datetime - sTime).TotalSeconds;
		}
		
		public static long ToMilliseconds(DateTime datetime) {
			return (long) datetime.ToUniversalTime().Subtract(
				new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
			).TotalMilliseconds;
		}
	}
}