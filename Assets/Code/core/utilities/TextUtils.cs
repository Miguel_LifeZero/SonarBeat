﻿using UnityEngine;

namespace core.utilities {
	public static class TextUtils {

		#region Parsing
		public static void CleanGameobjectName(GameObject gameobjToClean, string strToRemove) {
			gameobjToClean.name = gameobjToClean.name.Replace(strToRemove, "");
		}
		#endregion
	}
}