﻿using UnityEngine;

namespace core.utilities
{
    public class DDOL : MonoBehaviour {

        public void Awake() {
            DontDestroyOnLoad(gameObject);
        }
    }
}
