﻿using System.Collections;
using UnityEngine;

namespace core.utilities {
	public class TransformUtils {

		#region Destroy
		public static void DestroyAllChildren(Transform parent) {
			if(parent.childCount <= 0) return;
			for(var i = 0; i <= parent.childCount-1; ++i) {
				MonoBehaviour.Destroy(parent.GetChild(i).gameObject);
			}
		}

		public static void DestroyChildrenCondition(Transform parent, string nameToNotDestroy) {
			for(var i = 0; i <= parent.childCount-1; ++i) {
				if(!parent.GetChild(i).name.Contains(nameToNotDestroy)) {
					MonoBehaviour.Destroy(parent.GetChild(i).gameObject);
				}
			}
		}
		#endregion
	}
}