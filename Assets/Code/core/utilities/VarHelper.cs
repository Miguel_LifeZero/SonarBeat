﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace core.utilities {
    public class VarHelper {
        
        public static bool CheckNullOrEmpty<T>(T value) {
            if (typeof(T) == typeof(string))
                return string.IsNullOrEmpty(value as string);

            return value == null || value.Equals(default(T));
        }

        public static bool IsNull<T>(T value) {
            return ReferenceEquals(value, null);
        }

        public static bool IsNullImmediate<T>(T value) {
            return value == null || value.Equals(null);
        }

        public static bool IsNullOrZero<T>(T value) {
            return ReferenceEquals(value, null) || value.ToString() == "0" || value.ToString() == "-1";
        }

        public static string FirstCharToLower(string input){
            return char.ToLower(input[0]) + input.Substring(1);
        }

        public static string FirstCharToUpper(string input){
            return input.First().ToString().ToUpper() + input.Substring(1);
        }

        public static string CapitalizeFirstCharacter(string originalString) {
            originalString = char.ToUpper(originalString[0]) + originalString.Substring(1);
            return originalString;
        }

        public static string SplitClassName(string className){
            var classNameSplitted = className.Split('_');
            var finalName = FirstCharToLower(classNameSplitted[1]);
            return finalName;
        }

        public static List<T> RemoveNullSFromList<T>(List<T> list) {
            list.RemoveAll(item => item == null);
            return list;
        }

        public static string ToFormatNumber(int number){
            return string.Format("{0:n0}", number);
        }

        public static int ToIntFormatNumber(string formatNumber) {
            return Parse.IntParseFast(formatNumber.Replace(",00 ", "").Replace(",", "").Replace(".", ""));
        }

        public static string ShortNumber(long num) {
            var i = (long) Math.Pow(10, (int) Math.Max(0, Math.Log10(num) - 2));
            num = num / i * i;

            if (num >= 1000000000)
                return (num / 1000000000D).ToString("0.##") + "B";
            if (num >= 1000000)
                return (num / 1000000D).ToString("0.##") + "M";
            if (num >= 1000)
                return (num / 1000D).ToString("0.##") + "K";

            return num.ToString("#,0");
        }
        
        public static string FindTextValue(string textKey, IEnumerable<KeyValuePair<string, string>> keyValuePairList) {
            var finalName = "";
            foreach(var pair in keyValuePairList) {
                if(pair.Key == textKey) {
                    finalName = pair.Value;
                    break;
                }
            }
            return finalName;
        }
        
        public static Sprite GetSpriteImage(string spritesPath, string spriteName) {
            var allSprites = Resources.LoadAll<Sprite>(spritesPath).ToList();
            var spriteFound = allSprites.Find(spr => spr.name == spriteName);
            return spriteFound;
        }

        public static void CheckList<T>(List<T> values) {
            if (values.Count <= 0)
            {
                throw new Exception();
            }
        }

        public static void DisableEnableGameObject(GameObject gameObj) {
            gameObj.SetActive(false);
            gameObj.SetActive(true);
        }
    }
}