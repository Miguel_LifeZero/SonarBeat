﻿using core.configs;
using UnityEngine;
using Game;
using Radar;
using UnityEngine.UI;

namespace core.utilities {
	public static class Finder {

		#region Finders
		public static GameObject FindGameManager() {
			return GameObject.Find(Names.Scene.GameManager);
		}
		
		public static InputManager FindInputManager() {
			return GameObject.Find(Names.Scene.GameManager).GetComponent<InputManager>();
		}
		
		public static ObjectReference FindObjectReference() {
			return GameObject.Find(Names.Scene.Referencer).GetComponent<ObjectReference>();
		}
		
		public static CompassMovement FindCompassMovement() {
			return GameObject.Find(Names.Scene.Compass).GetComponent<CompassMovement>();
		}

		public static RadarBase FindRadarBase(){
			return GameObject.Find(Names.Scene.RadarBase).GetComponent<RadarBase>();
		}
		
		public static Text FindScoreText(){
			return GameObject.Find(Names.Scene.Score).GetComponent<Text>();
		}
		#endregion
	}
}