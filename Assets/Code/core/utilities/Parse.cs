﻿using System;

namespace core.utilities {
	public class Parse {
		
		public static int IntParseFast(string value) {
			var x = 0;
			int.TryParse(value, out x);
			return x;
		}

		public static bool StringToBool(string value) {
			return Convert.ToBoolean(Convert.ToInt32(value));
		}

		public static int StringToInt(string value) {
			return int.Parse(value);
		}
	}
}