﻿using UnityEngine;

namespace core.utilities {
    public static class RadarUtils {

        #region CircleCalculations
        public static float RandomAngleInRadar(int sections, int randomPosition) {
            return (0 + 360 / sections) * randomPosition;
        }
        
        public static Vector3 PlaceOnRadar(Vector3 center, float angle, float radius) {
            return
                new Vector3
                {
                    x = center.x + radius * Mathf.Sin(angle * Mathf.Deg2Rad),
                    y = center.y + radius * Mathf.Cos(angle * Mathf.Deg2Rad),
                    z = center.z
                };
        }
        #endregion
    }
}