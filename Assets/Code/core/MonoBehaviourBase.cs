﻿using System;
using System.Collections;
using System.Linq;
using core.utilities;
using UnityEngine;

namespace core {
    public class MonoBehaviourBase : MonoBehaviour {
        protected T AddComponent<T>() where T : Component {
            var component = gameObject.GetComponent<T>();
            if (VarHelper.IsNull(component)) {
                component = gameObject.AddComponent<T>();
            }
            return component;
        }

        public Component AddComponent(GameObject gameObj, string componentName) {
            return gameObj.AddComponent(Type.GetType(componentName));
        }

        public static bool HasComponent<T>(GameObject go) where T : Component {
            return go.GetComponentsInChildren<T>().FirstOrDefault() != null;
        }

        public static T AddOrGetComponent<T>(GameObject gameObj) where T : Component {
            var component = gameObj.GetComponent<T>();
            if (VarHelper.IsNull(component)) {
                component = gameObj.AddComponent<T>();
            }
            return component;
        }

        public static GameObject FindParentWithTag(GameObject childObject, string tag) {
            var t = childObject.transform;
            while (t.parent != null) {
                if (t.parent.CompareTag(tag)) {
                    return t.parent.gameObject;
                }
                t = t.parent.transform;
            }
            return null;
        }

        public static GameObject FindParentByName(GameObject childObject, string nameToSearch) {
            var t = childObject.transform;
            while (t.parent != null) {
                if (t.parent.name == nameToSearch) {
                    return t.parent.gameObject;
                }
                t = t.parent.transform;
            }
            return null;
        }
        
        public void ReviewException(Exception e){
            ////Debug.Log (e.Message);
        }

        protected void DestroyChildren(IEnumerable parent) {
            foreach (Transform child in parent) {
                Destroy(child.gameObject);
            }
        }

        protected void DestroyChildrenCondition(IEnumerable parent, string nameToNotDestroy) {
            foreach(Transform child in parent) {
                if(!child.name.Contains(nameToNotDestroy)) {
                    Destroy(child.gameObject);
                }
            }
        }
    }

    public static class TransformDeepChildExtension {
        //Breadth-first search
        public static Transform FindDeepChild(this Transform aParent, string aName) {
            var result = aParent.Find(aName);
            if (result != null)
                return result;
            foreach (Transform child in aParent) {
                result = child.FindDeepChild(aName);
                if (result != null)
                    return result;
            }
            return null;
        }
    }
}