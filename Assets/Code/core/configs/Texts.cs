﻿
namespace configs {
	public class Texts {

		public static readonly int NormalSize = 20;
		public static readonly int MaxSize = 20;
		public static readonly float FixedCharsOffset = 15f;
	}
}