﻿using UnityEngine;

namespace configs {
	public static class Colors {

		public static readonly Color Clear 			 = new Color(0.0f, 0.0f, 0.0f, 0.0f);
		public static readonly Color Black 			 = new Color32(0, 0, 0, 255);
		public static readonly Color White 			 = new Color32(255, 255, 255, 255);
		public static readonly Color Red 			 = new Color32(255, 0, 0, 255);
		public static readonly Color RedHighlight	 = new Color32(255, 160, 160, 255);
		public static readonly Color Orange 		 = new Color32(233,078,27,255);
		public static readonly Color Yellow 		 = new Color32(239, 182, 98, 255);
		public static readonly Color Gold 			 = new Color32(212, 155, 88, 255);
		public static readonly Color GreyDark 		 = new Color32(29, 29, 27, 255);
		public static readonly Color GreyLight 		 = new Color32(84, 84, 84, 255);
		public static readonly Color Grey 			 = new Color32(90, 94, 97, 175);
		public static readonly Color Green		 	 = new Color32(176, 206, 050, 255);
		public static readonly Color GreenHighlight	 = new Color32(175, 255, 175, 255);
		public static readonly Color BlueRandomCell  = new Color32(92, 176, 204, 200);
		public static readonly Color Blue 			 = new Color32(64, 104, 163, 255);
	}
}