﻿
namespace core.configs {
    public static class TagsAndLayers {
        
        #region SortingLayers
        public static class SortingLayers {
            public const string Background = "Background";
            public const string Default    = "Default";
            public const string Compass    = "Compass";
            public const string Enemies    = "Enemies";
            public const string Effects    = "Effects";
            public const string UI         = "UI";
        }
        #endregion
        
        #region PhysicsLayers
        public static class PhysicsLayers {
            public const int Radar     = 8;
            public const int Beats     = 9;
            public const int Metronome = 10;
        }
        #endregion
    }
}