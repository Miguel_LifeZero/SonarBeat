﻿
namespace core.configs {
	public static class Names {

		#region Scene
		public static class Scene {
			public const string GameManager = "_gameManager";
			public const string Referencer 	= "Referencer";
			public const string Compass	 	= "NavigationCompass";
			public const string RadarBase 	= "RadarBase";
			public const string Score 		= "Score";
		}
		#endregion
	
		#region Instantiating
		public static class GameObjects {
			public const string InstantiatedClone = "(Clone)";
		}
		#endregion
	}
}