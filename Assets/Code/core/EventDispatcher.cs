
namespace core {
	public static class EventDispatcher {

		#region Delegates
		//Song beats in radar touch event
		public delegate void BeatTouch();
		public static event BeatTouch BeatTouchEvent;
		//Song beats in radar release event
		public delegate void BeatRelease();
		public static event BeatRelease BeatReleaseEvent;
		//Player attack event
		public delegate void Attack();
		public static event Attack AttackEvent;
		#endregion

		#region BeatTouch event
		public static void OnBeatTouched() {
			if(BeatTouchEvent != null) {
				BeatTouchEvent();
			}
		}
		#endregion

		#region BeatRelease event
		public static void OnBeatReleased() {
			if(BeatReleaseEvent != null) {
				BeatReleaseEvent();
			}
		}
		#endregion

		#region Attack event
		public static void OnAttack() {
			if(AttackEvent != null) {
				AttackEvent();
			}
		}
		#endregion
	}
}